package com.rhinosystem.pacman;

/**
 * Created by gustavo on 26/07/15.
 */
public class Pacman {
    private int x=60;
    private int y=60;
    private String imagen;

    public Pacman() {
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
    public void moverArriva(){
        setY(getY()+30);
    }
    public void moverAbajo(){
        setY(getY()-30);
    }
    public void moverDerecha(){
        setX(getX() + 30);
    }
    public void moverIzquierda(){
        setX(getX() - 30);
    }


}
