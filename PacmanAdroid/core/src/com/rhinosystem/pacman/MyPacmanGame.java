package com.rhinosystem.pacman;


import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.utils.Timer;

import java.util.Random;

public class MyPacmanGame extends ApplicationAdapter  {
	SpriteBatch batch;
	Texture img,bloque,comida,luigi,fruta,fantasmaImg,fantasmaImg2,fantasmaImg3,fantasmaImg4;
	int x=30;
	int y=30;
	int laberinto[][] = new int[28][20];

	HorizontalGroup hg;
	VerticalGroup vg;

	TextButton optionButton,optionButton2,optionButton3,optionButton4;
	private Stage _stage;
	Table tblLayout;
	Pacman pacman;
	Fantasma fantasma1,fantasma2,fantasma3,fantasma4;
	private Music music,musicaFrutaCome,musicaMuertePacman;
	private float volume = 0.5f;
	Random randomGenerator;
	private  int randomInt ;
	private  int vidas=3 ;
	private  int putos=0 ;
	private  int contarTime=0 ;
	private boolean activarContarTime=false;
	private Texture splsh;
	private Game myGame;
	@Override
	public void create () {
		Skin skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
		batch = new SpriteBatch();
		pacman=new Pacman();
		fantasma1=new Fantasma();
		fantasma2=new Fantasma();
		fantasma3=new Fantasma();
		fantasma4=new Fantasma();
		int[] row2 = {0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0};
		int[] row3 = {0,1,1,1,1,0,1,1,1,1,1,0,1,1,3,0,0,0,1,0};
		int[] row4 = {0,1,2,1,1,1,1,0,1,0,1,1,1,1,1,0,0,0,1,0};
		int[] row5 = {0,1,2,0,0,0,1,0,1,0,1,1,0,1,1,0,0,0,1,0};
		int[] row6 = {0,1,2,0,0,1,1,0,1,0,1,1,0,1,1,1,1,0,1,0};
		int[] row7 = {0,1,2,1,0,1,1,1,1,1,1,1,0,1,1,0,1,0,1,0};
		int[] row8 = {0,1,2,2,2,2,1,2,0,2,3,1,0,0,0,1,1,3,1,0};
		int[] row9 = {0,1,0,1,0,1,0,2,0,0,2,1,1,1,1,1,1,0,1,0};
		int[] row10 = {0,2,0,1,1,0,1,2,0,0,2,1,1,1,1,1,1,0,1,0};
		int[] row11 = {0,2,0,1,1,1,1,2,0,0,2,1,1,1,1,1,1,0,1,0};
		int[] row12 = {0,2,1,1,1,1,1,2,0,0,2,1,1,1,1,1,1,1,1,0};
		int[] row13 = {0,2,1,1,0,1,0,1,0,1,1,0,0,1,1,1,1,0,0,0};
		int[] row14 = {0,2,0,1,0,1,0,1,1,1,1,1,1,0,1,1,1,1,1,0};
		int[] row15 = {0,2,0,1,0,1,0,2,2,2,2,1,0,0,1,1,3,1,1,0};
		int[] row16 = {0,2,0,1,0,1,0,2,0,0,0,1,0,0,0,1,0,1,1,0};
		int[] row17 = {0,2,1,1,1,1,1,2,0,1,0,1,1,2,0,1,0,1,1,0};
		int[] row18 = {0,2,1,0,1,0,0,2,2,2,2,0,0,2,2,1,3,1,1,0};
		int[] row19 = {0,2,1,1,0,0,0,0,0,0,0,2,2,0,2,1,1,1,1,0};
		int[] row20 = {0,2,0,1,1,1,1,1,1,1,1,1,0,0,2,1,1,0,1,0};
		int[] row21 = {0,2,1,0,2,2,2,2,2,2,2,2,0,1,2,1,1,0,2,0};
		int[] row22 = {0,2,1,3,1,0,0,1,0,0,0,0,0,1,0,1,0,0,2,0};
		int[] row23 = {0,2,1,0,1,0,1,1,0,0,2,2,2,2,2,1,0,0,2,0};
		int[] row24 = {0,2,2,0,2,2,2,2,2,2,2,3,0,0,0,1,1,1,2,0};
		int[] row25 = {0,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0};

		laberinto[1]=row2;
		laberinto[2]=row3;
		laberinto[3]=row4;
		laberinto[4]=row5;
		laberinto[5]=row6;
		laberinto[6]=row7;
		laberinto[7]=row8;
		laberinto[8]=row9;
		laberinto[9]=row10;
		laberinto[10]=row11;
		laberinto[11]=row12;
		laberinto[12]=row13;
		laberinto[13]=row14;
		laberinto[14]=row15;
		laberinto[15]=row16;
		laberinto[16]=row17;
		laberinto[17]=row18;
		laberinto[18]=row19;
		laberinto[19]=row20;
		laberinto[20]=row21;
		laberinto[21]=row22;
		laberinto[22]=row23;
		laberinto[23]=row24;

		pacman.setImagen("pacman.png");
		img = new Texture(pacman.getImagen());
		bloque= new Texture("roca2.bmp");
		comida= new Texture("comida.png");
		luigi=	 new Texture("Luigi.gif");
		fruta=	 new Texture("fruta.png");
		hg=new HorizontalGroup();
		vg=new VerticalGroup();
		boolean valor=true;
		tblLayout = new Table();
		musicaMuertePacman = Gdx.audio.newMusic(Gdx.files.getFileHandle("pacman_death.wav", FileType.Internal));
		musicaMuertePacman.setVolume(volume);
		musicaFrutaCome = Gdx.audio.newMusic(Gdx.files.getFileHandle("pacman_intermission.wav", FileType.Internal));
		musicaFrutaCome.setVolume(volume);
		music = Gdx.audio.newMusic(Gdx.files.getFileHandle("pacman_chomp.wav", FileType.Internal));
		music.setVolume(volume);

		/*Timer.schedule(new Timer.Task() {
			@Override
			public void run() {
				x=x+30;
			}
		}
				, 0, 1 / 50.0f);*/


		optionButton = new TextButton("Right", skin);
		optionButton2 =new TextButton("Left ", skin);
		optionButton3 = new TextButton("Up  ", skin);
		optionButton4 = new TextButton("Down", skin);
		optionButton.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if(laberinto[(pacman.getX()+30)/30][(pacman.getY())/30]!=0){
					pacman.setImagen("pacman.png");
					pacman.moverDerecha();

					if(laberinto[(pacman.getX()/30)][(pacman.getY())/30]==2){
						laberinto[(pacman.getX()/30)][(pacman.getY())/30]=1;

						music.play();
						putos=putos+5;

					}
					if(laberinto[(pacman.getX()/30)][(pacman.getY())/30]==3){
						laberinto[(pacman.getX()/30)][(pacman.getY())/30]=1;

						musicaFrutaCome.play();
						fantasma1.setImagen("fantasmaA.png");
						fantasma2.setImagen("fantasmaA.png");
						fantasma3.setImagen("fantasmaA.png");
						fantasma4.setImagen("fantasmaA.png");
						contarTime=0;
						activarContarTime=true;
						putos=putos+10;

					}

				}


			}

		});
		optionButton2.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (laberinto[(pacman.getX() - 30) / 30][(pacman.getY()) / 30] != 0) {
					pacman.setImagen("pacman2.png");
					pacman.moverIzquierda();
					if (laberinto[(pacman.getX() / 30)][(pacman.getY()) / 30] == 2){
						laberinto[(pacman.getX()/30)][(pacman.getY())/30]=1;

						music.play();
						putos=putos+5;

					}
					if(laberinto[(pacman.getX()/30)][(pacman.getY())/30]==3){
						laberinto[(pacman.getX()/30)][(pacman.getY())/30]=1;

						musicaFrutaCome.play();
						fantasma1.setImagen("fantasmaA.png");
						fantasma2.setImagen("fantasmaA.png");
						fantasma3.setImagen("fantasmaA.png");
						fantasma4.setImagen("fantasmaA.png");
						contarTime=0;
						activarContarTime=true;
						putos=putos+10;

					}
				}


			}

		});
		optionButton3.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (laberinto[(pacman.getX()) / 30][(pacman.getY() + 30) / 30] != 0) {
					pacman.setImagen("pacman3.png");
					pacman.moverArriva();
					if (laberinto[(pacman.getX() / 30)][(pacman.getY()) / 30] == 2){
						laberinto[(pacman.getX()/30)][(pacman.getY())/30]=1;

						music.play();
						putos=putos+5;

					}
					if(laberinto[(pacman.getX()/30)][(pacman.getY())/30]==3){
						laberinto[(pacman.getX()/30)][(pacman.getY())/30]=1;

						musicaFrutaCome.play();
						fantasma1.setImagen("fantasmaA.png");
						fantasma2.setImagen("fantasmaA.png");
						fantasma3.setImagen("fantasmaA.png");
						fantasma4.setImagen("fantasmaA.png");
						contarTime=0;
						activarContarTime=true;
						putos=putos+10;

					}
				}


			}

		});
		optionButton4.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (laberinto[(pacman.getX()) / 30][(pacman.getY() - 30) / 30] != 0) {
					pacman.setImagen("pacman4.png");
					pacman.moverAbajo();
					if (laberinto[(pacman.getX() / 30)][(pacman.getY()) / 30] == 2){
						laberinto[(pacman.getX()/30)][(pacman.getY())/30]=1;

						music.play();
						putos=putos+5;

					}
					if(laberinto[(pacman.getX()/30)][(pacman.getY())/30]==3){
						laberinto[(pacman.getX()/30)][(pacman.getY())/30]=1;

						musicaFrutaCome.play();
						fantasma1.setImagen("fantasmaA.png");
						fantasma2.setImagen("fantasmaA.png");
						fantasma3.setImagen("fantasmaA.png");
						fantasma4.setImagen("fantasmaA.png");
						contarTime=0;
						activarContarTime=true;
						putos=putos+10;

					}
				}


			}

		});


		optionButton.setHeight(60);
		optionButton.setWidth(60);
		optionButton2.setHeight(60);
		optionButton2.setWidth(60);
		optionButton3.setHeight(60);
		optionButton3.setWidth(60);
		optionButton4.setHeight(60);
		optionButton4.setWidth(60);
		hg.addActor(optionButton2);
		hg.addActor(optionButton);
		vg.addActor(optionButton3);
		vg.addActor(hg);
		vg.addActor(optionButton4);
		tblLayout.add(vg).width(90).height(90).space(100).row();
		tblLayout.setPosition(400,-200);



		_stage = new Stage();
		tblLayout.setFillParent(true);
		_stage.mouseMoved(600, 900);
		_stage.addActor(tblLayout);

		Gdx.input.setInputProcessor(_stage);
		/*music = Gdx.audio.newMusic(Gdx.files.getFileHandle("pacman_chomp.wav", FileType.Internal));
		music.setVolume(volume);
		music.play();*/
		//music.setLooping(true);

	}

	@Override
	public void render () {
		randomGenerator = new Random();
		randomInt = randomGenerator.nextInt(15);
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		img = new Texture(pacman.getImagen());
		fantasmaImg= new Texture(fantasma1.getImagen());
		batch.begin();


		for (int i = 0; i < laberinto.length; i++) {

			// Loop and display sub-arrays.
			int[] sub = laberinto[i];
			for (int x = 0; x < sub.length; x++) {
				if(sub[x]==0)
					batch.draw(bloque, i*30, x*30);
				if(sub[x]==2)
					batch.draw(comida, i*30, x*30);
				if(sub[x]==3)
					batch.draw(fruta, i*30, x*30);
			}
			System.out.println();
		}
		if(randomInt==1)
			if(laberinto[(fantasma1.getX()+30)/30][(fantasma1.getY())/30]!=0){
				fantasma1.moverDerecha();
			}
		if(randomInt==2)
			if(laberinto[(fantasma1.getX()-30)/30][(fantasma1.getY())/30]!=0){
				fantasma1.moverIzquierda();
			}
		if(randomInt==3)
			if(laberinto[(fantasma1.getX())/30][(fantasma1.getY()+30)/30]!=0){
				fantasma1.moverArriva();
			}
		if(randomInt==4)
			if(laberinto[(fantasma1.getX())/30][(fantasma1.getY()-30)/30]!=0){
				fantasma1.moverAbajo();
			}

		batch.draw(img, pacman.getX(), pacman.getY());
		batch.draw(fantasmaImg, fantasma1.getX(), fantasma1.getY());

		fantasmaImg2= new Texture(fantasma2.getImagen());
		randomGenerator = new Random();
		randomInt = randomGenerator.nextInt(15);
		if(randomInt==1)
			if(laberinto[(fantasma2.getX()+30)/30][(fantasma2.getY())/30]!=0){
				fantasma2.moverDerecha();
			}
		if(randomInt==2)
			if(laberinto[(fantasma2.getX()-30)/30][(fantasma2.getY())/30]!=0){
				fantasma2.moverIzquierda();
			}
		if(randomInt==3)
			if(laberinto[(fantasma2.getX())/30][(fantasma2.getY()+30)/30]!=0){
				fantasma2.moverArriva();
			}
		if(randomInt==4)
			if(laberinto[(fantasma2.getX())/30][(fantasma2.getY()-30)/30]!=0){
				fantasma2.moverAbajo();
			}
		batch.draw(fantasmaImg2, fantasma2.getX(),fantasma2.getY() );

		fantasmaImg3= new Texture(fantasma3.getImagen());
		randomGenerator = new Random();
		randomInt = randomGenerator.nextInt(15);
		if(randomInt==1)
			if(laberinto[(fantasma3.getX()+30)/30][(fantasma3.getY())/30]!=0){
				fantasma3.moverDerecha();
			}
		if(randomInt==2)
			if(laberinto[(fantasma3.getX()-30)/30][(fantasma3.getY())/30]!=0){
				fantasma3.moverIzquierda();
			}
		if(randomInt==3)
			if(laberinto[(fantasma3.getX())/30][(fantasma3.getY()+30)/30]!=0){
				fantasma3.moverArriva();
			}
		if(randomInt==4)
			if(laberinto[(fantasma3.getX())/30][(fantasma3.getY()-30)/30]!=0){
				fantasma3.moverAbajo();
			}
		batch.draw(fantasmaImg3, fantasma3.getX(),fantasma3.getY() );
		fantasmaImg4= new Texture(fantasma4.getImagen());
		randomGenerator = new Random();
		randomInt = randomGenerator.nextInt(15);
		if(randomInt==1)
			if(laberinto[(fantasma4.getX()+30)/30][(fantasma4.getY())/30]!=0){
				fantasma4.moverDerecha();
			}
		if(randomInt==2)
			if(laberinto[(fantasma4.getX()-30)/30][(fantasma4.getY())/30]!=0){
				fantasma4.moverIzquierda();
			}
		if(randomInt==3)
			if(laberinto[(fantasma4.getX())/30][(fantasma4.getY()+30)/30]!=0){
				fantasma4.moverArriva();
			}
		if(randomInt==4)
			if(laberinto[(fantasma4.getX())/30][(fantasma4.getY()-30)/30]!=0){
				fantasma4.moverAbajo();
			}
		batch.draw(fantasmaImg4, fantasma4.getX(), fantasma4.getY());
		batch.end();
		_stage.act();
		Vector2 v=new Vector2();
		v.scl(300, 300);
		_stage.screenToStageCoordinates(v);
		_stage.mouseMoved(600, 900);

		_stage.draw();
		if(activarContarTime)
			contarTime++;
		if(contarTime>70){
			fantasma1.setImagen("fantasmaM.png");
			fantasma2.setImagen("fantasmaM.png");
			fantasma3.setImagen("fantasmaM.png");
			fantasma4.setImagen("fantasmaM.png");
			contarTime=0;
			activarContarTime=false;
		}
		if(pacman.getX()==fantasma1.getX()&&pacman.getY()==fantasma1.getY()){
			if(!activarContarTime){
				vidas--;
				musicaMuertePacman.play();
				pacman.setX(60);
				pacman.setY(60);
				pacman.setImagen("pacman.png");
			}else {
				fantasma1.setX(360);
				fantasma1.setY(390);
				fantasma1.setImagen("fantasmaM.png");
			}
		}
		if(pacman.getX()==fantasma2.getX()&&pacman.getY()==fantasma2.getY()){
			if(!activarContarTime){
				vidas--;
				musicaMuertePacman.play();
				pacman.setX(60);
				pacman.setY(60);
				pacman.setImagen("pacman.png");
			}else {
				fantasma2.setX(360);
				fantasma2.setY(390);
				fantasma2.setImagen("fantasmaM.png");
			}

		}
		if(pacman.getX()==fantasma3.getX()&&pacman.getY()==fantasma3.getY()){
			if(!activarContarTime){
				vidas--;
				musicaMuertePacman.play();
				pacman.setX(60);
				pacman.setY(60);
				pacman.setImagen("pacman.png");
			}else {
				fantasma3.setX(360);
				fantasma3.setY(390);
				fantasma3.setImagen("fantasmaM.png");
			}
		}
		if(pacman.getX()==fantasma4.getX()&&pacman.getY()==fantasma4.getY()){
			if(!activarContarTime){
				vidas--;
				musicaMuertePacman.play();
				pacman.setX(60);
				pacman.setY(60);
				pacman.setImagen("pacman.png");
			}else {
				fantasma4.setX(360);
				fantasma4.setY(390);
				fantasma4.setImagen("fantasmaM.png");
			}
		}


		SpriteBatch spriteBatch;
		BitmapFont font;
		CharSequence str = "Vidas: "+vidas;
		CharSequence str2 = "Puntos: "+putos;
		CharSequence str3 = "Tiempo: "+contarTime;
		spriteBatch = new SpriteBatch();

		font = new BitmapFont();
		spriteBatch.begin();
		font.draw(spriteBatch, str, 850, 580);
		font.draw(spriteBatch, str2, 850, 540);


		font.draw(spriteBatch, str3, 850, 500);

		spriteBatch.end();



	}
	@Override
	public void resize(int width, int height) {
		_stage.getViewport().update(width, height);
	}
	@Override
	public void dispose() {
		//se elimina recursos
		music.dispose();

	}
}
